import express from 'express'
import CourseService from '../services/course.service.js'

const coursesRouter = express.Router()

coursesRouter.post('/course', async (req, res) => {
    const result = await CourseService.add(req.body)
    return res.json(result)
})

export default coursesRouter
