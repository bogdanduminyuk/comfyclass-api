import loaders from './loaders/index.js'
import express from 'express'
import coursesRouter from './api/course.routes.js'

async function startServer() {
    const port = process.env.PORT || 4000

    const app = express()

    await loaders(app)

    app.use(coursesRouter)

    app.use((req, res, next) => {
        res.status(404)
        res.json({ error: 'Error 404: not found' })
    })

    app.listen(port, (err) => {
        if (err) {
            console.log(err)
            return
        }

        console.log('Server is ready')
    })
}

startServer()
