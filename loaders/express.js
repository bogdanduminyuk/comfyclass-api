import express from 'express'
import path from 'path'

export default async (app, staticfilesDirname) => {
    app.use(express.static(path.resolve(staticfilesDirname)))
    app.use(express.json())
    app.use(express.urlencoded({ extended: false }))

    app.get('/status', (req, res) => res.status(200).end())

    return app
}
