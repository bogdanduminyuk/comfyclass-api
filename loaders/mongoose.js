import mongoose from 'mongoose'
import * as config from '../config/index.js'

export default async () => {
    const options = {
        useNewUrlParser: true,
        useUnifiedTopology: true,
        useCreateIndex: true,
    }

    mongoose.Promise = global.Promise

    console.log(config.DB_URI)
    const connection = await mongoose.connect(config.DB_URI, options)

    return connection.connection.db
}
