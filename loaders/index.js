import expressLoader from './express.js'
import mongooseLoader from './mongoose.js'

export default async (app) => {
    const mongoConnection = await mongooseLoader()
    console.log('MongoDB is initialized')

    await expressLoader(app, 'static')
    console.log('Express is initialized')
}
