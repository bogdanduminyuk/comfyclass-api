import CourseModel from '../models/course.model.js'

export default class CourseService {
    static async add(courseJson) {
        let newcourse = await CourseModel.create(courseJson)
        console.log(newcourse)
        newcourse = newcourse.toObject()
        delete newcourse.__v
        delete newcourse._id
        return newcourse
    }
}
