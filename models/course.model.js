import mongoose from 'mongoose'

const CourseSchema = mongoose.Schema({
    parent: {
        type: mongoose.Types.ObjectId,
        index: true,
        default: null,
    },

    name: {
        type: String,
        required: true,
        minlegth: 20,
        maxLength: 80,
    },

    description: String,

    author: {
        type: mongoose.Types.ObjectId,
        default: null,
    },

    img: String,

    public: Boolean,

    price: {
        type: Number,
        min: 0,
    },

    date: {
        type: Date,
        min: '2000-01-01',
        max: '01/01/2100',
        default: Date.now,
    },
})

export default mongoose.model('Course', CourseSchema)
